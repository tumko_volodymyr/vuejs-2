export const userMapper = user => ({
    id: user.id,
    email: user.email,
    name: user.name,
});

export const albumMapper = album => ({
    id: album.id,
    userId: album.userId,
    title: album.title,
});
