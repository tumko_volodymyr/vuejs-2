import Vue from 'vue'
import Vuex from 'vuex'
import user from './store/modules/user'
import album from './store/modules/album'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    album,
  },
})
