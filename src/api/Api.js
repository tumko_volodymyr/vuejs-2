import axios from 'axios/index';

class Api {
    constructor(apiUrl) {
        this.axios = axios.create({ baseURL: apiUrl });

        this.axios
            .interceptors
            .response
            .use(
                response => response.data,
                errorResponse => {
                    const { response } = errorResponse;

                    if (!response) {
                        return Promise.reject(new Error('Unexpected error!'));
                    }

                    const error = response.data.errors[0];
                    return Promise.reject(error);
                },
            );
    }

    get(url, params) {
        return this.axios.get(url, { params });
    }

    post(url, data) {
        return this.axios.post(url, data);
    }

    put(url, data) {
        return this.axios.put(url, data);
    }

    delete(url, params) {
        return this.axios.delete(url, params);
    }
}

export default new Api(process.env.VUE_APP_API_URL);
