import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('./views/user/Users.vue')
    },
    {
      path: '/users/add',
      name: 'add-user',
      component: () => import('./views/user/NewUser.vue')
    },
    {
      path: 'users/:id/edit',
      name: 'edit-user',
      component: () => import('./views/user/UpdateUser.vue')
    },
    {
      path: 'users/:id',
      name: 'show-user',
      component: () => import('./views/user/ShowUser.vue')
    },
    {
      path: '/albums',
      name: 'albums',
      component: () => import('./views/album/Albums.vue')
    },
  ]
})
