import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import Buefy from 'buefy';
import 'buefy/dist/buefy.css'
Vue.use(Buefy, {
  defaultIconComponent: 'font-awesome-icon'
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
