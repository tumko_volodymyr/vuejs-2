
export default {
    getAlbums: state => () => state.albums,

    getAlbum: state => id => {
        return  state.albums[id]},
};
