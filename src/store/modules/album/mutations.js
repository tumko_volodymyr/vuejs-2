import {SET_ALBUM, SET_ALBUMS, ADD_ALBUM, DELETE_ALBUM}  from './mutationTypes';
import {albumMapper} from '@/services/Normalizer';

export default {
    [SET_ALBUMS]: (state, albums) => {
        albums.forEach(album => {
            state.albums = {
                ...state.albums,
                [album.id]: albumMapper(album)
            };
        });
    },

    [ADD_ALBUM]: (state, album) => {
        state.albums = {
            ...state.albums,
            [album.id]: albumMapper(album)
        };
    },

    [DELETE_ALBUM]: (state, id) => {
        const albums = { ...state.albums };
        delete albums[id];
        state.albums = albums ;
    },


    [SET_ALBUM]: (state, album) => {
        state.albums = {
            ...state.albums,
            [album.id]: albumMapper(album)
        };
    },
};
