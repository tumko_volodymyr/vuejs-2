import api from '@/api/Api';
import {SET_ALBUM, SET_ALBUMS, ADD_ALBUM, DELETE_ALBUM} from './mutationTypes';

export default {
    async fetchAlbums({ commit }) {
        try {
            const albums = await api.get('/albums');
            commit(SET_ALBUMS, albums);
            return Promise.resolve(albums);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async addAlbum({ commit }, { email, name }) {

        try {
            const album = await api.post('/albums', { email, name});
            commit(ADD_ALBUM, album);
            return Promise.resolve(album);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async deleteAlbum({ commit }, id) {
        try {
            await api.delete(`/albums/${id}`);

            commit(DELETE_ALBUM, id);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async updateAlbum({ commit }, { id, name, email }) {
        try {

            const album = await api.put(`/albums/${id}`, { name, email});

            commit(SET_ALBUM, album);
            return Promise.resolve(album);
        } catch (error) {
            return Promise.reject(error);
        }
    },
};
