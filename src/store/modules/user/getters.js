
export default {
    getUsers: state => () => state.users,

    getUser: state => id => {
        return  state.users[id]},
};
