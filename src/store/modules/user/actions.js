import api from '@/api/Api';
import {SET_USER, SET_USERS, ADD_USER, DELETE_USER} from './mutationTypes';

export default {
    async fetchUsers({ commit }) {
        try {
            const users = await api.get('/users');
            commit(SET_USERS, users);
            return Promise.resolve(users);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async addUser({ commit }, { email, name }) {

        try {
            const user = await api.post('/users', { email, name});
            commit(ADD_USER, user);
            return Promise.resolve(user);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async deleteUser({ commit }, id) {
        try {
            await api.delete(`/users/${id}`);

            commit(DELETE_USER, id);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async updateUser({ commit }, { id, name, email }) {
        try {

            const user = await api.put(`/users/${id}`, { name, email});

            commit(SET_USER, user);
            return Promise.resolve(user);
        } catch (error) {
            return Promise.reject(error);
        }
    },
};
