import {SET_USER, SET_USERS, ADD_USER, DELETE_USER} from './mutationTypes';
import {userMapper} from '@/services/Normalizer';

export default {
    [SET_USERS]: (state, users) => {
        users.forEach(user => {
            state.users = {
                ...state.users,
                [user.id]: userMapper(user)
            };
        });
    },

    [ADD_USER]: (state, user) => {
        state.users = {
            ...state.users,
            [user.id]: userMapper(user)
        };
    },

    [DELETE_USER]: (state, id) => {
        const users = { ...state.users };
        delete users[id];
        state.users = users ;
    },


    [SET_USER]: (state, user) => {
        state.users = {
            ...state.users,
            [user.id]: userMapper(user)
        };
    },
};
